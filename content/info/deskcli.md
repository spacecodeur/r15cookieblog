---
title: "Desktop Command Line"
draft: false
tags: [
    "doku-import",
    "cli",
    "ops"
]
date: "2012-08-17"
meta: "false"
---

A complete desktop environment utilizing the command line

- SSH Access
  - Pure SSH
  - Javascript SSH for public console, with SSO
- Command Environment
  - bash
  - screen to allow easy multi-tasking, or perhaps [byobu](https://launchpad.net/byobu)
- Utilities
  - links: Browser
  - https://www.passwordstore.org/
  - finch: IM
  - [Task Warrior](http://taskwarrior.org/) Command line task manager
  - [LedgerCLI](http://www.ledger-cli.org/) and [CSV2Ledger](https://github.com/jwiegley/CSV2Ledger|CSV2Ledger): Command-line accounting
  - resty: REST interface (could be useful for automating web info, without using links)
  - irssi: IRCgfs

## Articles

- [15 Tools to Customize Your Terminal and Command Line](https://www.shopify.com/partners/blog/customize-terminal)
- [Terminal Codes](https://wiki.bash-hackers.org/scripting/terminalcodes) Useful for fancy things like bold, blink, terminal colors, etc.
